DMX LED Dimmers
================

This repository contains designs for DMX512 controlled LED dimmers. The LEDs can be of any type from 6-30V.
The dimmers can be configured using a USB connected PC - there are plans to use RDM as well.

(c) Konrad Rosenbaum, 2023
protected under the GNU GPL v.3
see COPYING.txt for details

Directories:

kicad -> 9x 8bit PWM channels, plus 3 switch channels, TO-220 transistors

lowCur_kicad -> as above (identical schematic), but using smaller SOT-23 transistors, for (slightly) lower current and much smaller footprint

fwsrc -> firmware sources

doc -> documentation, open doc/index.html in a browser
