// AVR Firmware Library
// (c) Konrad Rosenbaum, 2022/23
// protected under the GNU GPLv3

#pragma once

///\file
/// \ref RingBuffer definition.

#include <stdint.h>

///Ring buffer implementation.
///\param Char character type, should be an integer type, must be comparable
///\param length the number of Char in this buffer, must be positive between 0..127
///\param nullchar the null character that terminates strings (and implicitly lines)
///\param newline the newline character(s) that terminate(s) lines
template <class Char,uint8_t length,Char nullchar,Char ...newline>
class RingBuffer
{
    static_assert(length<=127 && length>=2, "RingBuffer size must be 2..127 items");
private:
    //internal character buffer
    Char mbuf[length]={};
    //start position and length of buffered data in characters
    uint8_t mpos=0,mlen=0;
    //true if the current line is being discarded
    bool minvalid=false;

    //helper for detecting end of line
    template<typename ...Args>
    inline bool isOneOf(Char c,Char one,Args...args)const{return c==one || isOneOf(c,args...);}
    inline bool isOneOf(Char c,Char one)const{return c==one;}
    inline bool isLineEnd(Char c)const{return isOneOf(c,nullchar,newline...);}

public:
    ///Instantiates a new ring buffer.
    constexpr RingBuffer()=default;

    ///returns the number of characters currently in the buffer
    uint8_t size()const{return mlen;}
    ///returns the number of characters for which there is still space in the buffer
    uint8_t spaceLeft()const{return length-mlen;}
    ///returns the number of characters that can be stored at maximum
    static constexpr uint8_t maxSize(){return length;}

    ///returns true if the buffer is already full
    bool isFull()const{return mlen==length;}
    ///returns true if the buffer is empty
    bool isEmpty()const{return mlen==0;}
    ///returns true if the buffer stores at least one complete line
    bool hasLine()const
    {
        for(uint8_t p=0;p<mlen;p++){
            const auto c = mbuf[(mpos+p)%length];
            if(isLineEnd(c))return true;
        }
        return false;
    }

    ///Gets the length of the next line in the buffer.
    ///If there is a full line
    uint8_t lineSize()const
    {
        for(int8_t p=0;p<mlen;p++){
            const auto c = mbuf[(mpos+p)%length];
            if(isLineEnd(c))return p;
        }
        return 0;
    }

    ///get a char, but don't take it from the buffer
    ///\param pos position of the char relative to the start
    ///\returns the character at that position of a null character if the character does not exist
    Char peek(uint8_t pos=0)const
    {
        if(pos>=mlen || pos<0)
            return nullchar;
        else
            return mbuf[(mpos+mlen+pos)%length];
    }

    ///get the next character from the buffer or a null character if there is none
    Char get(){
        if(mlen==0)return nullchar;
        Char r=mbuf[mpos];
        mpos++;mlen--;
        if(mpos>=length || mlen==0)mpos=0;
        return r;
    }

    ///Get the next line.
    ///If there is no complete line or arguments make no sense it returns nullptr.
    ///It may return fewer than max characters. If max is not enough for the entire line then only the first max characters
    ///will be returned and the remainder of the line will be discarded.
    ///\param buf character buffer that must have space for at least max+1 chars, this may change even if nullptr is returned; on success the string in buf will be terminated with nullchar
    ///\param max the maximum number of characters that will be returned (per default equal to the ring buffer length)
    ///\returns a pointer to buf on success or nullptr on failure
    Char* getLine(Char*buf,int8_t max=length)
    {
        if(buf==nullptr || max<=0 || mlen<1)
            return nullptr;
        //find line
        buf[max]=nullchar;
        for(uint8_t p=0;p<mlen;p++){
            //next char
            const auto c=mbuf[(mpos+p)%length];
            //copy just in case this is a line
            if(p<max)
                buf[p] = isLineEnd(c) ? nullchar : c;
            //check for end of line (EOL is implied when the buffer is full)
            if(isLineEnd(c) || p==(length-1)){
                //correct position/length
                mpos += p+1;
                mpos %= length;
                mlen -= p+1;
                if(mlen==0)mpos=0;
                //return result
                return buf;
            }
        }
        //not found:
        return nullptr;
    }

    ///Append a character to the buffer.
    ///\returns true on success, false if the buffer is already full or in invalid line mode.
    ///\note ignores characters while in invalid line mode; switches back to valid line mode if the character is a newline or nullchar
    bool append(Char c)
    {
        if(minvalid){
            if(isLineEnd(c))minvalid=false;
            return false;
        }
        if(mlen==length)return false;
        mbuf[(mpos+mlen)%length]=c;
        mlen++;
        return true;
    }

    ///Append a string to the buffer.
    ///Tries to append the entire string, up to, but except for the nullchar at the end.
    ///\param s string to be added
    ///\returns the number of characters that were added or -1 if the string was invalid.
    ///\note if in invalid line mode ignores all characters up to the first newline
    int8_t appendStr(const Char*s)
    {
        if(s==nullptr)return -1;
        int8_t cnt=0;
        while(!isFull()){
            const auto c=*s++;
            if(c==nullchar)return cnt;
            if(append(c))cnt++;
            else break;
        }
        return cnt;
    }

    ///Append a string to the buffer.
    ///Tries to append the entire string of length len, including nullchars.
    ///\param s string to be added
    ///\param len number of characters to be added
    ///\returns the number of characters that were added or -1 if the string was invalid.
    ///\note if in invalid line mode ignores all characters up to the first newline
    int8_t appendStr(const Char*s, int8_t len)
    {
        if(s==nullptr || len<0)return -1;
        int8_t cnt=0;
        while(!isFull() && len>0){
            const auto c=*s++;
            len--;
            if(append(c))cnt++;
            else break;
        }
        return cnt;
    }

    ///Enters invalid line mode.
    ///Deletes all characters at the back of the buffer back to the last newline or nullchar.
    ///All subsequently added characters are ignored up to the next newline or nullchar.
    void invalidateLine()
    {
        //delete end
        while(mlen>0){
            const auto c = mbuf[(mpos+mlen-1)%length];
            if(isLineEnd(c))return;
            else mlen--;
        }
        //enter invalid mode
        minvalid=true;
    }

    ///Returns true if the buffer contains a specific character.
    bool contains(Char c)
    {
        if(mlen==0)return false;
        for(uint8_t p=0;p<mlen;p++)
            if(mbuf[(mpos+p)%length]==c)
                return true;
        return false;
    }
};
