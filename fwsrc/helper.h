//Helper functions
//(c) Konrad Rosenbaum, 2024
//protected under GNU GPL v.3 or at your option any newer

#pragma once

///helper to output values as hex
static inline void tohex1(char*buf,uint8_t num)
{
    static const char hex[]="0123456789ABCDEF";
    if(!buf)return;
    buf[0]=hex[num>>4];
    buf[1]=hex[num&0xf];
    buf[2]=0;
}

///helper to output decimal value
static inline void todec1(char*buf,uint8_t val)
{
    if(buf==nullptr)return;
    if(val>=100){
        buf[0]='0'+(val/100);
        buf[1]='0'+(val/10%10);
        buf+=2;
    }else if(val>=10){
        buf[0]='0'+(val/10);
        buf++;
    }
    buf[0]='0'+(val%10);
    buf[1]=0;
}

///helper to output decimal value
static inline void todec2(char*buf,uint16_t val)
{
    if(buf==nullptr)return;
    uint8_t digs=1;
    for(uint16_t t=val/10;t>0;digs++)t/=10;
    buf[digs]=0;
    for(;digs>0;digs--){
        buf[digs-1]=val%10+'0';
        val/=10;
    }
}
