//LED Output Handling
//(c) Konrad Rosenbaum, 2023
//protected under GNU GPL v.3 or at your option any newer

#include "ledout.h"
#include "pindefs.h"

void ledoutinit()
{
    //configure pins
    DMXACTPORT.DIRSET = DMXACTPIN;
    DMXACTPORT.DMXACTCTRL = 0;
    DMXACTPORT.OUTCLR = DMXACTPIN;

    SW10PORT.DIRSET = SW10PIN;
    SW10PORT.SW10CTRL = 0;
    SW10PORT.OUTCLR = SW10PIN;

    SW11PORT.DIRSET = SW11PIN;
    SW11PORT.SW11CTRL = 0;
    SW11PORT.OUTCLR = SW11PIN;

    SW12PORT.DIRSET = SW12PIN;
    SW12PORT.SW12CTRL = 0;
    SW12PORT.OUTCLR = SW12PIN;
}

static inline void switchMe(PORT_t&port,uint8_t pin,SwitchMode sm)
{
    switch(sm){
        case SwitchMode::On:
            port.OUTSET = pin;
            break;
        case SwitchMode::Off:
            port.OUTCLR = pin;
            break;
        case SwitchMode::Toggle:
            if(port.OUT&pin)port.OUTCLR = pin;
            else port.OUTSET = pin;
            break;
        default: break;
    }
}

void dmxactivity(SwitchMode sm)
{
    switchMe(DMXACTPORT,DMXACTPIN,sm);
}

void switchChannel(uint8_t channel,SwitchMode mode)
{
    switch(channel){
        case SwitchChannelNumber::Sw10: switchMe(SW10PORT,SW10PIN,mode);break;
        case SwitchChannelNumber::Sw11: switchMe(SW11PORT,SW11PIN,mode);break;
        case SwitchChannelNumber::Sw12: switchMe(SW12PORT,SW12PIN,mode);break;
        default:break;
    }
}

bool channelState(uint8_t channel)
{
    switch(channel){
        case SwitchChannelNumber::Sw10: return (SW10PORT.OUT & SW10PIN) != 0;
        case SwitchChannelNumber::Sw11: return (SW11PORT.OUT & SW11PIN) != 0;
        case SwitchChannelNumber::Sw12: return (SW12PORT.OUT & SW12PIN) != 0;
        default:return false;
    }
}
