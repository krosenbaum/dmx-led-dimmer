// AVR Firmware Library
// (c) Konrad Rosenbaum, 2022/23
// protected under the GNU GPLv3

#pragma once

///\file
/// UART communication to host.

#include <stdint.h>

///Encapsulates the UART to communicate with the host.
class Uart
{
public:
    ///Initialize the UART
    Uart();

    ///send a plain string
    void send(const char*);
    ///send multiple strings
    template<typename... Args>
    inline void send(const char*s,Args...args){send(s);send(args...);}
    ///send a plain string and wait for it to be transmitted
    inline void sendWait(const char*s,uint8_t thr=8){send(s);waitForOutput(thr);}
    ///send multiple strings
    template<typename... Args>
    inline void sendWait(Args...args){send(args...);waitForOutput();}

    ///callback function pointer type
    typedef void (*callback_t)(const char*);
    ///sets a callback for reading data
    void setCallback(callback_t cmdcall,uint8_t maxcmd);

    ///process buffered input data
    void processData();

    ///wait for all data to be written
    ///\param thr threshold for send queue, returns when queue is shorter than this
    void waitForOutput(uint8_t thr=8);

    ///return how many bytes are in the input buffer
    uint8_t inputSize()const;
    ///return how many bytes are in output buffer
    uint8_t outputSize()const;
    ///return how many bytes of space are left in output buffer
    uint8_t outputSpace()const;

    ///read a number of bytes from input buffer
    ///\param buf the buffer to read into
    ///\param max the amount of bytes that can be read into the buffer
    ///\returns the number of bytes actually read
    ///\note does not add terminating null bytes, this must be done in the calling code
    uint8_t read(char*buf,uint8_t max);

    ///set echo mode
    ///\param on if true: switch echo mode on; if false: echo off
    void setEcho(bool on);
    ///returns true if echo mode is on
    bool isEchoOn()const;

    ///checks that the input buffer contains a specific character
    bool inputContains(char);

    ///initialize Uart
    static void init();
};

extern Uart uart;
