// AVR Firmware Library
// (c) Konrad Rosenbaum, 2022-24
// protected under the GNU GPLv3

///\file
/// UART communication to host.

#include "uart.h"
#include "pindefs.h"
#include "ringbuf.h"

#include <avr/interrupt.h>
#include <util/atomic.h>
#include <util/delay.h>
#include <string.h>

//buffers n' stuff

const uint8_t nullchar=0, newline='\n', carreturn='\r';

///Internal Data Structure of UART
struct UartData {
    RingBuffer<uint8_t,64,nullchar,newline,carreturn> in;
    RingBuffer<uint8_t,127,nullchar,newline> out;
    int8_t maxcmd=3;
    Uart::callback_t call=nullptr;
    bool isSending=false;
    bool isEchoOn=false;
};
static UartData uartdata;

//Uart code

static const uint32_t BAUDVAL = 4L * F_CPU / USBSPEED;

Uart::Uart()
{
}

void Uart::init()
{
    //USB serial
    USBUART.BAUD = BAUDVAL; // async, normal mode
    USBUART.CTRLC = // async, 8N1:
        (uint8_t)USART_CMODE_ASYNCHRONOUS_gc |
        (uint8_t)USART_PMODE_DISABLED_gc |
        (uint8_t)USART_CHSIZE_8BIT_gc |
        (uint8_t)USART_SBMODE_1BIT_gc;
    USBPORT.DIRSET = PIN0_bm;//TXD out
    USBPORT.DIRCLR = PIN1_bm;//RXD in
    USBPORT.PIN0CTRL = 0; //no inverter, no pullup, no interrupt
    USBPORT.PIN1CTRL = 0;
    USBUART.CTRLB = USART_RXEN_bm | USART_TXEN_bm | USART_RXMODE_NORMAL_gc; //Rx Enabled, Tx Enabled, Normal Speed, No Wake on UART, No Open Drain, No Multi-MCU Comm Mode
    USBUART.CTRLA = USART_RXCIE_bm ; //interrupt for Receive (Send: DRE=Data Register Empty will be set by send routine)
    //done
}

void Uart::setCallback(Uart::callback_t cmdcall, uint8_t maxcmd)
{
    uartdata.call=cmdcall;
    uartdata.maxcmd=maxcmd;
}

void Uart::send(const char*str)
{
    const size_t len=strlen(str);
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        //check space
        if(uartdata.out.spaceLeft()<len)return;
        //queue it
        uartdata.out.appendStr((uint8_t*)str, len);
        //start sending if not already sending (unless nothing to send)
        if(!uartdata.isSending && !uartdata.out.isEmpty()){
            uartdata.isSending = true; // remember we are sending now
            USBUART.CTRLA |= USART_DREIE_bm; // enable more sending (activate send interrupt)
            //interrupt handler will schedule the actual data
        }
    }
}


void Uart::processData()
{
    //skip if there is no callback
    if(!uartdata.call)return;
    //process it
    char buf[uartdata.maxcmd+1];
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        //handle receive buffer
        if(uartdata.in.isEmpty())return;
        //parse data
        if(uartdata.in.getLine((uint8_t*)buf,uartdata.maxcmd)==nullptr)return;
    }
    //make call
    if(uartdata.call)
        uartdata.call(buf);
}

static bool isprintable(uint8_t c)
{
    //actual printable chars (space..max 7bit)
    if(c>=32 && c<=127)return true;
    //not printable
    return false;
}

///receive UART data
ISR(USB_RXC_vect, ISR_BLOCK)
{
    //get data and store it (or discard it)
    do{
        //check RXCIF bit (ignore all others)
        uint8_t dat=0;
        if((USBUART.STATUS & USART_RXCIF_bm)==0)break;
        //get data
        dat=USBUART.RXDATAL;
        //store data
        uartdata.in.append(dat);
        if(dat=='#')uartdata.in.invalidateLine();
        //echo?
        if(uartdata.isEchoOn && isprintable(dat)){
            uartdata.out.append(dat);
            if(!uartdata.isSending){
                uartdata.isSending = true;
                USBUART.CTRLA |= USART_DREIE_bm;
            }
        }
    }while(true);
}

///UART: data register empty: transmit more!
ISR(USB_DRE_vect, ISR_BLOCK)
{
    //check for data
    if(uartdata.out.isEmpty()){
        uartdata.isSending=false; // no longer sending
        USBUART.CTRLA &= ~USART_DREIE_bm; // disable next send interrupt
        return;
    }
    //schedule it
    USBUART.TXDATAL = uartdata.out.get();
}

void Uart::waitForOutput(uint8_t thr)
{
    while(uartdata.isSending && uartdata.out.size()>thr)
        _delay_us(10);
}

uint8_t Uart::inputSize() const
{
    return uartdata.in.size();
}

uint8_t Uart::outputSize() const
{
    return uartdata.out.size();
}

uint8_t Uart::outputSpace() const
{
    return uartdata.out.spaceLeft();
}

uint8_t Uart::read(char* buf, uint8_t max)
{
    if(buf==nullptr || max==0)return 0;
    uint8_t i;
    for(i=0;i<max;i++){
        if(uartdata.in.isEmpty())break;
        buf[i]=uartdata.in.get();
    }
    return i;
}

void Uart::setEcho(bool on)
{
    uartdata.isEchoOn=on;
}

bool Uart::isEchoOn() const
{
    return uartdata.isEchoOn;
}

bool Uart::inputContains(char c)
{
    return uartdata.in.contains(c);
}

//allocate Uart
Uart uart;
