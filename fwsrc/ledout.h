//LED Output Handling
//(c) Konrad Rosenbaum, 2023-24
//protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <stdint.h>

///initialize LED output
void ledoutinit();

///How to switch a channel
enum class SwitchMode {
    ///Switch LED off
    Off,
    ///Switch LED on
    On,
    ///Toggle on/off state of LED
    Toggle
};

///Switch the special DMX activity LED
void dmxactivity(SwitchMode sm=SwitchMode::Toggle);

///Helper Enum for Channel numbers (use these instead of integers, so that you get compiler errors instead of silent failure)
enum SwitchChannelNumber {
    ///Channel 10
    Sw10 = 10,
    ///Channel 11
    Sw11 = 11,
    ///Channel 12
    Sw12 = 12
};
///First LED switch channel number
#define MIN_SWITCH_CHANNEL 10
///Last LED switch channel number
#define MAX_SWITCH_CHANNEL 12

///Switch an LED channel
///\param channel channel number between MIN_SWITCH_CHANNEL and MAX_SWITCH_CHANNEL, fails silently for non-existing channels
///\param mode which way to switch, see \ref SwitchMode
void switchChannel(uint8_t channel,SwitchMode mode);
///returns current channel state
///\param channel channel number between MIN_SWITCH_CHANNEL and MAX_SWITCH_CHANNEL, fails silently for non-existing channels
///\returns true for ON, false for OFF; always returns false for non-existing channels
bool channelState(uint8_t channel);
