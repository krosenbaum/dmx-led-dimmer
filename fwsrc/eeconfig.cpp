//EEPROM configuration
//(c) Konrad Rosenbaum, 2024
//protected under GNU GPL v.3 or at your option any newer

#include "eeconfig.h"
#include "uart.h"
#include "dmx.h"
#include "pwm.h"
#include "pindefs.h"
#include "ledout.h"
#include "helper.h"

#include <avr/eeprom.h>
#include <string.h>

#define CFG_MAGIC 0xCA
#define CFG_VERSION 0x01

struct Config{
    uint8_t magic;
    uint8_t version;
    //version 1:
    uint16_t dmxoffset;
    uint8_t channels[12];
    uint8_t dmxdirection;
    //not yet here: version 2
};

static Config mainconfig = {.magic=0, .version=0};

static EEMEM Config eepconfig;

void initconfig()
{
    //get from EEPROM
    readconfig();
    //force on widget
    resetconfig();
}

///re-read config from EEPROM
void readconfig(Config&cfg)
{
    eeprom_read_block(&cfg,&eepconfig,sizeof(Config));
}

void readconfig()
{
    readconfig(mainconfig);
    //check config is valid -> if not: reset to defaults
    if(mainconfig.magic != CFG_MAGIC){
        mainconfig.magic=CFG_MAGIC;
        mainconfig.version=CFG_VERSION;
        memset(mainconfig.channels,0,sizeof(mainconfig.channels));
        mainconfig.dmxoffset=1;
        mainconfig.dmxdirection=(uint8_t)DmxDirection::Receive;
    }
}

void resetconfig()
{
    if(mainconfig.magic!=CFG_MAGIC)return;
    //version 1: has DMX offset and channels
    setdmxoffset(mainconfig.dmxoffset);
    dmxdirection((DmxDirection)mainconfig.dmxdirection);
    for(uint8_t ch=0;ch<PWM_CHANNELS;ch++)
        pwmset(ch,mainconfig.channels[ch]);
    for(uint8_t ch=MIN_SWITCH_CHANNEL;ch<=MAX_SWITCH_CHANNEL;ch++)
        switchChannel(ch, mainconfig.channels[ch-1]>127 ? SwitchMode::On : SwitchMode::Off);
    // //version 2: ??
    // if(mainconfig.magic>1)...
}

void saveconfig(bool channels)
{
    //reset if not yet set
    if(mainconfig.magic!=CFG_MAGIC || mainconfig.version==0 || mainconfig.version==0xff)
        memset(&mainconfig,0,sizeof(Config));
    //init
    mainconfig.magic=CFG_MAGIC;
    mainconfig.version=CFG_VERSION;
    //fetch values
    mainconfig.dmxoffset=getdmxoffset();
    if(channels){
        for(uint8_t i=0;i<PWM_CHANNELS;i++)mainconfig.channels[i]=pwmvalue(i);
        for(uint8_t i=MIN_SWITCH_CHANNEL;i<=MAX_SWITCH_CHANNEL;i++)
            mainconfig.channels[i-1] = channelState(i) ? 0xFF : 0;
    }
    //save to EEPROM
    eeprom_write_block(&mainconfig,&eepconfig,sizeof(Config));
}

void printconfig(const Config&cfg)
{
    if(cfg.magic != CFG_MAGIC || cfg.version==0 || cfg.version==0xff){
        uart.sendWait("Config structure not valid.");
        return;
    }
    char buf[8];
    uart.send("Config Version: ");
    todec1(buf,cfg.version);
    uart.send(buf);
    uart.sendWait("\r\nDMX Offset: ");
    todec2(buf,cfg.dmxoffset);
    uart.send(buf);
    for(uint8_t ch=0;ch<sizeof(cfg.channels);ch++){
        uart.sendWait("\r\nChannel ");
        todec1(buf,ch+1);
        uart.send(buf);
        uart.send(" @ ");
        todec1(buf,cfg.channels[ch]);
        uart.send(buf);
    }
}

void printconfig(bool eeprom)
{
    if(eeprom){
        Config c;
        readconfig(c);
        printconfig(c);
    }else
        printconfig(mainconfig);
}
