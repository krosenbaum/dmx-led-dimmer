#!/bin/bash

echo Regenerating version.h ... 1>&2


echo '//AUTO-GENERATED FILE'
echo '#pragma once'

echo '///GIT Commit of current Firmware'
echo '#define VERSION_COMMIT "'$(git log -1 --format=%H . )'"'

echo '///GIT Version date of current Firmware'
echo '#define VERSION_DATE "'$(git log -1 --format=%cs . )'"'

DIRTY=$(git status -s . | wc -l)
echo '///GIT Modification State (==0: unmodified; >0: has modifications)'
echo '#define VERSION_DIRTY' $DIRTY

echo Done. 1>&2
