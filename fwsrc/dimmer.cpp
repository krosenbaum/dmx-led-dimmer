//Dimmer Main Program
//(c) Konrad Rosenbaum, 2023-24
//protected under GNU GPL v.3 or at your option any newer


#include "pindefs.h"

#include <util/delay.h>

#include "dmx.h"
#include "pwm.h"
#include "ledout.h"
#include "uart.h"
#include "command.h"
#include "eeconfig.h"

#include <avr/interrupt.h>

///initialize pins and basic settings
void initchip()
{
    //configure clock source (writing to MCLKCTRL* needs to free the CPU config lock first: via CCP register)
    CCP = CCP_IOREG_gc;
    CLKCTRL.MCLKCTRLA = MCLKCTRLA_VAL;
    CCP = CCP_IOREG_gc;
    CLKCTRL.MCLKCTRLB = MCLKCTRLB_VAL;

    //config UARTs
    UARTMUXREG = UARTMUXVAL;
    Uart::init();
    dmxinit();

    //init other modules
    ledoutinit();
    pwminit();
    cmdinit();

    //start configuration
    initconfig();

    //enable interrupts
    sei();
}

///Main Loop
int main(void)
{
    initchip();

    for(;;){
        uart.processData();
        pwmloop();
    }

    return 0;
}

/** \mainpage
 *
 * This firmware implements several capabilities for the DMX LED Dimmer widget:
 *
 * * \ref DMX512 via the dmx.h and dmx.cpp file.
 * * A command protocol for USB configuration via command.h and command.cpp.
 * * The UART via which USB commands are received via uart.h, uart.cpp and the Uart class.
 * * PWM controlled dimmer ports via pwm.h and pwm.cpp.
 * * Switching ports via ledout.h and ledout.cpp.
 * * Timer and clock functions in rtc.h and rtc.cpp.
 * * Central definitions and chip selection are done in pindefs.h.
 * * Initialization and the main command loop are found in dimmer.cpp.
 * * The ringbuf.h file contains a utility RingBuffer class for data queuing.
 *
 * When the firmware starts up it first calls \ref initchip to initialize all components, which in turn calls init
 * functions from all sub-modules. It then enters the \ref main loop.
 *
 * Most hardware is interrupt driven or works independent of the main loop. Some basic processes are called from the
 * main loop to keep data flowing between modules.
 */
