//EEPROM Configuration
//(c) Konrad Rosenbaum, 2024
//protected under GNU GPL v.3 or at your option any newer

#pragma once

///initialize configuration and default channel values from EEPROM and reset the widget (calls readconfig and resetconfig)
void initconfig();

///re-read config from EEPROM
void readconfig();

///reset widget settings from current configuration
void resetconfig();

///save configuration to EEPROM
///\param channels if true the current channel values will be saved as new default
void saveconfig(bool channels=false);

///print config to UART
///\param eeprom if true: print config stored in EEPROM, if false: print current
void printconfig(bool eeprom=false);
