//DMX receiver
//(c) Konrad Rosenbaum, 2023-24
//protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <stdint.h>

///initialize DMX port
void dmxinit();

///Direction of the DMX port
enum class DmxDirection {
    ///Receive mode (default): receiving data via DMX will set the Channel outputs
    Receive = 0,
    ///Send mode: Channels are set via serial commands only and copied to DMX output, behaves as controller
    Send = 1,
    ///DMX is completely off and has no control over Channels
    Off = 2,
};

///get direction of DMX port
DmxDirection dmxdirection();

///set direction for DMX port, also used to switch DMX off
void dmxdirection(DmxDirection);

///set the DMX offset
void setdmxoffset(uint16_t);

///get the DMX offset
uint16_t getdmxoffset();

///print current DMX state
void printdmx();
