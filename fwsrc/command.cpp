//Dimmer Command Module
//(c) Konrad Rosenbaum, 2023-24
//protected under GNU GPL v.3 or at your option any newer

///\file
///Command handler.
///The command handler connects directly to the UART (see \ref uart.h and \ref uart.cpp).
///It then receives ASCII commands from the serial port and handles those.

#include "pindefs.h"

#include "command.h"
#include "uart.h"
#include "pwm.h"
#include "ledout.h"
#include "fwversion.h"
#include "helper.h"
#include "eeconfig.h"
#include "dmx.h"

#include <string.h>
#include <ctype.h>

///terminal mode while communicating via serial port
enum class Mode {
    ///human interaction (default) - echos input and prints a prompt
    Human,
    ///batch communication, no echo, no extraneous output
    Batch
};
///terminal mode, see \ref Mode
static Mode termmode=Mode::Human;

///helper to print the MCU serial number
static void printserial()
{
    char buf[21];
    tohex1(buf+0 ,SIGROW.SERNUM0);
    tohex1(buf+2 ,SIGROW.SERNUM1);
    tohex1(buf+4 ,SIGROW.SERNUM2);
    tohex1(buf+6 ,SIGROW.SERNUM3);
    tohex1(buf+8 ,SIGROW.SERNUM4);
    tohex1(buf+10,SIGROW.SERNUM5);
    tohex1(buf+12,SIGROW.SERNUM6);
    tohex1(buf+14,SIGROW.SERNUM7);
    tohex1(buf+16,SIGROW.SERNUM8);
    tohex1(buf+18,SIGROW.SERNUM9);
    uart.send(buf);
}

///handles info command
///\note the format of the output should stay the same, so it can be parsed by PC apps
static void printinfo()
{
    uart.sendWait("DMX Dimmer Gadget by Konrad Rosenbaum\r\nSerial Number: ");
    printserial();
    uart.sendWait("\r\nFirmware Version: " VERSION_DATE " Commit " VERSION_COMMIT
#if VERSION_DIRTY > 0
                " (modified)"
#endif
    );
    uart.sendWait("\r\nWeb: https://gitlab.com/krosenbaum/dmx-led-dimmer");
}

///handles help command without parameter
static void printhelp()
{
    uart.send("HELP TOPICS: help, info, dmx, set, get, echo, config\r\ntype \"help <topic>\" for more.");
}

///handles help command with parameters
static void printhelp2(const char*topic)
{
    while(isblank(*topic))topic++;
    if(strcmp(topic,"help")==0)
        uart.send("help <topic>\r\n\tprint help about a command");
    else if(strcmp(topic,"info")==0)
        uart.send("info\r\n\tprint info about this gadget");
    else if(strcmp(topic,"dmx")==0)
        uart.send("dmx\r\n\tprint current DMX settings\r\ndmx offset <num>\r\n\tset offset to 1..512\r\ndmx send|receive|off\r\n\tset DMX mode");
    else if(strcmp(topic,"set")==0)
        uart.send("set <channel> <value>\r\n\tset channel to new output value\r\n\t<channel> = 1..12\r\n\t<value> = 0..255 or on/off");
    else if(strcmp(topic,"get")==0)
        uart.send("get\r\n\tget all channel values\r\nget <channel>\r\n\tget value of specific channel (1..12)");
    else if(strcmp(topic,"echo")==0)
        uart.send("echo\r\n\tget current echo mode\r\necho on\r\n\tset human interaction mode with echo\r\necho off\r\n\tset batch mode");
    else if(strcmp(topic,"config")==0)
        uart.send("config\r\n\tprint currently active config\r\nconfig eep\r\n\tprint config from EEPROM\r\nconfig save [values]\r\n\tsave current config (if 'values': include current channel states) to EEPROM\r\nconfig reset\r\n\treset widget from config");
    else
        uart.send("Sorry, no such topic.");
}

///handles echo command without parameter, also called from \ref setecho
static void printecho()
{
    uart.send("echo is ");
    uart.send(uart.isEchoOn()?"on":"off");
}

///handles echo command with parameter
static void setecho(const char*echo)
{
    while(isblank(*echo))echo++;
    if(strcmp(echo,"on")==0){
        uart.setEcho(true);
        termmode=Mode::Human;
    }else if(strcmp(echo,"off")==0){
        uart.setEcho(false);
        termmode=Mode::Batch;
    }
    printecho();
}

///called from \ref getall and \ref getone(const char*) to display a single channel
static void getone(uint8_t ch)
{
    char buf[30];
    if(ch>=1 && ch<=9){
        buf[0]=ch+'0';
        buf[1]=' ';
        buf[2]='@';
        buf[3]=' ';
        todec1(buf+4,pwmvalue(ch-1));
    }
    else if(ch>=10 && ch<=12){
        buf[0]='1';
        buf[1]=ch-10+'0';
        buf[2]=' ';
        buf[3]='@';
        buf[4]=' ';
        strcpy(buf+5,channelState(ch)?"on":"off");
    }
    else strcpy(buf,"Error: unknown channel");
    uart.sendWait(buf);
}

///handles get command without parameter
static void getall()
{
    for(uint8_t i=1;i<=12;i++){
        getone(i);
        if(i<12)uart.send("\r\n");
    }
}

///handles get command with parameter
static void getone(const char*chs)
{
    while(isblank(*chs))chs++;
    uint8_t ch=0;
    while(*chs){
        if(*chs>='0' && *chs<='9'){
            ch *= 10;
            ch += *chs - '0';
        }else{
            uart.send("Invalid channel number.");
            return;
        }
        chs++;
    }
    getone(ch);
}

///handles set command - set one channel value
static void setone(const char*data)
{
    while(isblank(*data))data++;
    //channel
    uint8_t ch=0;
    while(*data){
        if(*data>='0' && *data<='9'){
            ch *= 10;
            ch += *data - '0';
        }else if(isblank(*data)){
            break;
        }else{
            uart.send("Invalid channel number.");
            return;
        }
        data++;
    }
    if(ch<1 || ch>12){
        uart.send("Invalid channel.");
        return;
    }
    //value
    while(isblank(*data))data++;
    uint8_t val=0;
    if(strcmp(data,"on")==0)
        val=255;
    else if(strcmp(data,"off")==0)
        val=0;
    else
        while(*data){
            if(*data>='0' && *data<='9'){
                val *= 10;
                val += *data - '0';
            }else if(isblank(*data)){
                break;
            }else{
                uart.send("Invalid value.");
                return;
            }
            data++;
        }
    //set
    if(ch<10)
        pwmset(ch-1,val);
    else
        switchChannel(ch, val>127 ? SwitchMode::On : SwitchMode::Off);
    //confirm
    getone(ch);
}

///handles complex DMX command
static void dmxcommand(const char*data)
{
    while(isblank(*data))data++;
    if(*data == 0)printdmx();
    else if(strncmp(data,"offset ",7)==0){
        data+=7;
        while(isblank(*data))data++;
        uint16_t val=0;
        while(*data){
            if(*data>='0' && *data<='9'){
                val *= 10;
                val += *data - '0';
            }else if(isblank(*data)){
                break;
            }else{
                uart.send("Invalid value.");
                return;
            }
            data++;
        }
        if(val<1 || val>512)
            uart.send("invalid offset");
        else{
            setdmxoffset(val);
            uart.send("Offset ");
            char buf[6];
            todec2(buf,getdmxoffset());
            uart.sendWait(buf);
        }
    }
    else if(strcmp(data,"send")==0)dmxdirection(DmxDirection::Send);
    else if(strcmp(data,"receive")==0)dmxdirection(DmxDirection::Receive);
    else if(strcmp(data,"off")==0)dmxdirection(DmxDirection::Off);
    else
        uart.send("Unknown DMX command");
}

///handles config command
static void configcommand(const char*data)
{
    while(isblank(*data))data++;
    if(*data == 0)printconfig();
    else if(strcmp(data,"eep")==0)printconfig(true);
    else if(strcmp(data,"reset")==0)resetconfig();
    else if(strcmp(data,"save")==0)saveconfig();
    else if(strcmp(data,"save values")==0)saveconfig(true);
    else
        uart.send("Unknown config command");
}

///command handler callback - called automatically from UART
void cmdhandler(const char*buf)
{
    uart.send("\r\n");
    if(*buf==0){
        if(termmode==Mode::Human)uart.send("Command> ");
        return;
    }
    if(strcmp(buf,"help")==0 || strcmp(buf,"?")==0)printhelp();
    else if(strncmp(buf,"help ",5)==0)printhelp2(buf+4);
    else if(strcmp(buf,"info")==0)printinfo();
    else if(strcmp(buf,"echo")==0)printecho();
    else if(strncmp(buf,"echo ",5)==0)setecho(buf+5);
    else if(strcmp(buf,"get")==0)getall();
    else if(strncmp(buf,"get ",4)==0)getone(buf+4);
    else if(strncmp(buf,"set ",4)==0)setone(buf+4);
    else if(strcmp(buf,"dmx")==0 || strncmp(buf,"dmx ",4)==0)dmxcommand(buf+3);
    else if(strcmp(buf,"config")==0 || strncmp(buf,"config ",7)==0)configcommand(buf+6);
    else uart.send("Sorry, unknown command. Try again or try \"help\".");

    uart.send("\r\n.\r\n");
    if(termmode==Mode::Human)
        uart.send("Command> ");
}

void cmdinit()
{
    uart.setCallback(&cmdhandler,32);
    uart.setEcho(true);
}
