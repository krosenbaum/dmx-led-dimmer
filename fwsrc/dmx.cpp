//DMX receiver
//(c) Konrad Rosenbaum, 2022-24
//protected under GNU GPL v.3 or at your option any newer

#include "dmx.h"
#include "pindefs.h"
#include "ledout.h"
#include "pwm.h"
#include "uart.h"
#include "helper.h"

#include <avr/interrupt.h>
#include <util/atomic.h>

/** \page DMX512 The DMX512 Protocol
 * \tableofcontents
 * The DMX512 protocol is defined by <a href="https://www.esta.org/">ESTA</a> <a href="https://tsp.esta.org/tsp/documents/published_docs.php">standard E1.11</a>.
 *
 * Each DMX frame begins with a Break (low) and Mark-after-Break (MAB, high) followed by up to 513 slots.
 * Slot 0 defines the type of message that is being sent, normally a NULL (value 0) frame for DMX dimmer data.
 * Slot 1..512 correspond to the DMX channels being set by the controller.
 * There is no lower limit on the number of slots.
 *
 * On the sender side the Break must be between 92 and 176µs long. The MAB between 12µs and 1s. This can be
 * accomplished by a baud rate at least 3× lower and transmitting a 0x00 octet.
 *
 * On the receiver side a packet is valid with a Break between 88 and 176µs and an MAB between 8µs and 1s.
 * We can detect the break condition as a frame error.
 *
 * Each slot has 1 start bit, 8 data bits and 2 stop bits (UART protocol, 8N2).
 * The baud rate is 250000 (bit/s raw or 4µs per bit). Slots may have no distance (mark time) or up to 1s distance.
 * There is no requirement for for a silent phase (mark) before the next Break condition.
 *
 * There must be at least one NULL frame per second and a maximum of 44 NULL frames per second with a minimum
 * distance of 1204µs (sender) or 1196µs (receiver). This distance becomes relevant in case of transmission
 * errors, since slots have to be ignored for this time.
 *
 *
 */

///DMX offset
static uint16_t dmxoffset = 1;

///DMX direction
static DmxDirection dmxdir = DmxDirection::Receive;

///Magic values for DMX read position
namespace DmxReadPos {
    enum DmxReadPos{
        NewFrame = 0,
        FirstValue = 1,
        LastValue = 512,
        NoFrame = 0x7fff,
    };
}

///DMX read offset
static uint16_t dmxreadpos = DmxReadPos::NoFrame;

///DMX baud rate
#define DMXBAUD 250000L

DmxDirection dmxdirection()
{
    return dmxdir;
}

void dmxdirection(DmxDirection d)
{
    if((uint8_t)d<0 || (uint8_t)d>2)return;
    dmxdir=d;
    if(d==DmxDirection::Send)
        DMXPORT.OUTSET = 0x80;
    else
        DMXPORT.OUTCLR = 0x80;
}

void dmxinit()
{
    //configure UART for DMX (250 kBaud, 8N2)
    DMXUART.BAUD = (long)F_CPU * 64L / DMXBAUD / 16 ;
    DMXUART.CTRLC = (uint8_t)USART_CMODE_ASYNCHRONOUS_gc | (uint8_t)USART_PMODE_DISABLED_gc | (uint8_t)USART_CHSIZE_8BIT_gc | (uint8_t)USART_SBMODE_2BIT_gc; // async, 8N2
    DMXPORT.DIRSET = PIN4_bm | PIN7_bm;//TXD out, Direction out
    DMXPORT.DIRCLR = PIN5_bm;//RXD in
    DMXPORT.PIN4CTRL = 0;
    DMXPORT.PIN5CTRL = 0;
    DMXPORT.PIN7CTRL = 0;
    DMXUART.CTRLB = USART_RXEN_bm | USART_TXEN_bm | USART_RXMODE_NORMAL_gc; //Rx Enabled, Tx Enabled, Normal Speed, No Wake on UART, No Open Drain, No Multi-MCU Comm Mode
    DMXUART.CTRLA = USART_RXCIE_bm ; //interrupt for Receive
    //CTRLA |= USART_DREIE_bm -> will be done in send routine (DRE=Data Register Empty)

    //configure direction, default mode: receive
    dmxdirection(dmxdir);
    dmxactivity();//FIXME
}

///receive UART data
ISR(DMX_RXC_vect, ISR_BLOCK)
{
    //get data and store it (or discard it)
    do{
        //check RXCIF bit (ignore all others)
        if((DMXUART.STATUS & USART_RXCIF_bm)==0)break;
        //get data
        uint8_t flags=DMXUART.RXDATAH;
        uint8_t dat=DMXUART.RXDATAL;
        (void)dat;
        //store data
        if(dmxdir!=DmxDirection::Receive)continue;
        if(flags&USART_FERR_bm){
            //frame error means break detected, reset the reader
            dmxreadpos = DmxReadPos::NewFrame;
            continue;
        }
        if(dmxreadpos==DmxReadPos::NewFrame){
            if(dat != 0){//first byte must be 0
                dmxreadpos = DmxReadPos::NoFrame;
                continue;
            }
        }else if(dmxreadpos > DmxReadPos::LastValue){
            //ignore values after 512 slots
            continue;
        }else{
            //is this one of mine?
            if(dmxreadpos>=dmxoffset){
                uint8_t ch=dmxreadpos-dmxoffset;
                if(ch<PWM_CHANNELS)pwmset(ch,dat);
                else{
                    ch++;
                    if(ch>=MIN_SWITCH_CHANNEL && ch<=MAX_SWITCH_CHANNEL)
                        switchChannel(ch,dat>127 ? SwitchMode::On : SwitchMode::Off);
                }
            }
        }
        dmxreadpos++;
        //TODO: introduce timers
    }while(true);
}

///UART: data register empty: transmit more!
ISR(DMX_DRE_vect, ISR_BLOCK)
{
    //schedule it
    //DMXUART.TXDATAL = '#';
    //disable next interrupt
    DMXUART.CTRLA &= ~USART_DREIE_bm;
}


void dmxloop()
{
}

void setdmxoffset(uint16_t off)
{
    if(off>=1 && off<=512)dmxoffset=off;
}

uint16_t getdmxoffset()
{
    return dmxoffset;
}

void printdmx()
{
    uart.send("DMX is ");
    switch(dmxdir){
        case DmxDirection::Receive: uart.send("receiving");break;
        case DmxDirection::Send:    uart.send("sending");break;
        case DmxDirection::Off:     uart.send("off");break;
        default:                    uart.send("confused");break;
    }
    uart.send("\r\nOffset ");
    char buf[16];
    todec2(buf,dmxoffset);
    uart.sendWait(buf);
}
