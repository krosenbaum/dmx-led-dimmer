//Dimmer Command Module
//(c) Konrad Rosenbaum, 2023-24
//protected under GNU GPL v.3 or at your option any newer

#pragma once

///\file
///Header for command handling connected to the serial port. See \ref command.cpp for details.

///initialize command handler (remainder happens via callback)
void cmdinit();
