//PWM Handling
//(c) Konrad Rosenbaum, 2022-24
//protected under GNU GPL v.3 or at your option any newer

#include "pindefs.h"

#include "pwm.h"

#include <string.h>

template <typename T>
class RegRef{
    T& ref;
public:
    constexpr RegRef(T&reg):ref(reg){}
    inline T& data()const{return ref;}
    inline operator T&()const{return ref;}
    inline RegRef<T>& operator=(T v){ref=v;return *this;}
};


RegRef<register8_t> pwm_cmp[PWM_CHANNELS] = {
    TCA0.SPLIT.LCMP0,
    TCA0.SPLIT.LCMP1,
    TCA0.SPLIT.LCMP2,
    TCA0.SPLIT.HCMP0,
    TCA0.SPLIT.HCMP1,
    TCA0.SPLIT.HCMP2,
    TCB0.CCMPH,
    TCB1.CCMPH,
    TCB2.CCMPH,
};


#define PWM_MAX 0xFF
#define PWM_DEFAULT 0

static uint8_t pwm_setval[PWM_CHANNELS];
static uint8_t pwm_curval[PWM_CHANNELS];

void pwminit()
{
    //send TCA/B to the right output
    PORTMUX.TCAROUTEA = TCAMUXVAL; //PWM TCA
    PORTMUX.TCBROUTEA = TCBMUXVAL; //PWM TCB

    //set port to output
    PORTA.DIRSET = 0b00001100; //Ch7/8
    PORTC.DIRSET = 0b00000011; //Ch9/10
    PORTD.DIRSET = 0b00111111; //Ch1-6

    /////
    //Configure TCA
    //enable split mode on TCA
    TCA0.SPLIT.CTRLD=1;
//     1. Write a TOP value to the Period (TCAn.PER) register.
    TCA0.SPLIT.HPER=PWM_MAX;
    TCA0.SPLIT.LPER=PWM_MAX;
//     2. Enable the peripheral by writing a ‘1’ to the ENABLE bit in the Control A (TCAn.CTRLA) register.
//     The counter will start counting clock ticks according to the prescaler setting in the Clock Select (CLKSEL) bit
//     field in the TCAn.CTRLA register.
    TCA0.SPLIT.CTRLA = TCA_SPLIT_CLKSEL_DIV64_gc | 1; //enabled, max speed
//     3. Optional: By writing a ‘1’ to the Enable Count on Event Input (CNTEI) bit in the Event Control (TCAn.EVCTRL)
//     register, events are counted instead of clock ticks.
    //leave at 0
//     4. The counter value can be read from the Counter (CNT) bit field in the Counter (TCAn.CNT) register.
    //enable compares
    TCA0.SPLIT.CTRLB =  TCA_SPLIT_LCMP0EN_bm | TCA_SPLIT_LCMP1EN_bm | TCA_SPLIT_LCMP2EN_bm |
                        TCA_SPLIT_HCMP0EN_bm | TCA_SPLIT_HCMP1EN_bm | TCA_SPLIT_HCMP2EN_bm;


    //////
    // Configure TCBx
    TCB0.CCMPL = PWM_MAX;
    TCB0.CCMPH = PWM_DEFAULT;
    TCB0.CTRLA |= TCB_CLKSEL_CLKTCA_gc | TCB_ENABLE_bm ;
    TCB0.CTRLB |= TCB_CCMPEN_bm | TCB_CNTMODE_PWM8_gc ;

    TCB1.CCMPL = PWM_MAX;
    TCB1.CCMPH = PWM_DEFAULT;
    TCB1.CTRLA |= TCB_CLKSEL_CLKTCA_gc | TCB_ENABLE_bm ;
    TCB1.CTRLB |= TCB_CCMPEN_bm | TCB_CNTMODE_PWM8_gc ;

    TCB2.CCMPL = PWM_MAX;
    TCB2.CCMPH = PWM_DEFAULT;
    TCB2.CTRLA |= TCB_CLKSEL_CLKTCA_gc | TCB_ENABLE_bm ;
    TCB2.CTRLB |= TCB_CCMPEN_bm | TCB_CNTMODE_PWM8_gc ;

    //////
    //init set values
    memset(pwm_setval,PWM_DEFAULT,sizeof(pwm_setval));
    memset(pwm_curval,0,sizeof(pwm_curval));
}

void pwmloop()
{
    for(int8_t i=0 ; i<PWM_CHANNELS ; i++)
        if(pwm_curval[i] != pwm_setval[i]){
            pwm_cmp[i] = pwm_setval[i];
            pwm_curval[i] = pwm_setval[i];
        }
}

uint8_t pwmvalue(uint8_t output)
{
    if(output<0 || output>=PWM_CHANNELS)return 0;
    return pwm_setval[output];
}

void pwmset(uint8_t output, uint8_t setval)
{
    if(output<0 || output>=PWM_CHANNELS)return;
    pwm_setval[output] = setval;
}
