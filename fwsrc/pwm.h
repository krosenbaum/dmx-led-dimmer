//Hårte PWM Handling
//(c) Konrad Rosenbaum, 2022
//protected under GNU GPL v.3 or at your option any newer

#pragma once

///initialize PWM subsystem
void pwminit();
///regularly update PWM values
void pwmloop();

///get current value of a PWM channel
uint8_t pwmvalue(uint8_t output);
///set value for PWM channel, updeted during next \ref pwmloop
void pwmset(uint8_t output, uint8_t setval);
