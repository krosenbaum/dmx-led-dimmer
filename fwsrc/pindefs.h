//DMX Dimmer Pin Definitions
//(c) Konrad Rosenbaum, 2023
//protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <avr/io.h>

//hack to convince KDevelop to load the correct MCU header
#ifndef _AVR_IOXXX_H_
#warning "Ooops. This should not leak beyond KDev."
#include <avr/iom3208.h>
#endif


/* Pins   Function
 * 22 A0: -
 * 23 A1: -
 * 24 A2: TCB0-WO, PWM, Ch7
 * 25 A3: TCB1-WO, PWM, Ch8
 * 26 A4: USART0-Tx, DMX
 * 27 A5: USART0-Rx, DMX
 * 28 A6: USART0-XCK, -
 *  1 A7: USART0-XDIR, DMX
 *
 *  - B*: not exist
 *
 *  2 C0: TCB2-WO, PWM, Ch9
 *  3 C1: GPIO Out, Ch10
 *  4 C2: -
 *  5 C3: GPIO Out, DMX Activity LED
 *
 *  - C*: not exist
 *
 *  6 D0: TCA0-WO0, PWM, Ch1
 *  7 D1: TCA0-WO1, PWM, Ch2
 *  8 D2: TCA0-WO2, PWM, Ch3
 *  9 D3: TCA0-WO3, PWM, Ch4
 * 10 D4: TCA0-WO4, PWM, Ch5
 * 11 D5: TCA0-WO5, PWM, Ch6
 * 12 D6: GPIO Out, Ch11
 * 13 D7: GPIO Out, Ch12
 *
 * 14 AVdd
 * 15 GND
 *
 *  - E*: not exist
 *
 * 16 F0: USART2-Tx, USB
 * 17 F1: USART2-Rx, USB
 *
 *  - F*: not exist
 *
 * 18 F6: -
 *
 * 19 UPDI               UPDI-Connector
 * 20 Vdd                UPDI-Connector/Input
 * 21 GND                UPDI-Connector/Input
 */

//Clock Config
//F_CPU is defined in Makefile, next to clock fuse settings
#define MCLKCTRLA_VAL 0x0 //no clockout (0x80), use internal 16/20MHz (0x0-0x3)
#define MCLKCTRLB_VAL 0x0 //no prescaler (PEN=0), (DIV=0x0)


//LED Pins and Switch Pins
#define DMXACTPORT PORTC
#define DMXACTPIN PIN3_bm
#define DMXACTCTRL PIN3CTRL

#define SW10PORT PORTC
#define SW10PIN  PIN1_bm
#define SW10CTRL PIN1CTRL
#define SW11PORT PORTD
#define SW11PIN  PIN6_bm
#define SW11CTRL PIN6CTRL
#define SW12PORT PORTD
#define SW12PIN  PIN7_bm
#define SW12CTRL PIN7CTRL

//PWM Config (using TCA/TCB)
#define PWM_CHANNELS 9

////
//Port MUX Config

//PWM routing
// TCA: use PORTD
#define TCAMUXVAL PORTMUX_TCA0_PORTD_gc
// TCB0: PA2 (default=PA2, alt=PF4)
// TCB1: PA3 (default=PA3, alt=PF5)
// TCB2: PC0 (default=PC0, alt=PB4)
// TCB3: (does not exist on XX08, only on XX09 chips)
#define TCBMUXVAL PORTMUX_TCB0_DEFAULT_gc | PORTMUX_TCB1_DEFAULT_gc | PORTMUX_TCB2_DEFAULT_gc ;

//UART MUX helpers
#define UART_DEFAULT_PINS 0
#define UART_ALT1_PINS 1
#define UART_NO_PINS 3
#define UART0_MUX 0
#define UART1_MUX 2
#define UART2_MUX 4
#define UART3_MUX 6

//UART routing (UART0 -> PA[7:4]; UART2 -> PF[3:0]; No UART1/3)
#define UARTMUXREG PORTMUX_USARTROUTEA
#define UARTMUXVAL \
    UART_ALT1_PINS      << UART0_MUX | \
    UART_NO_PINS        << UART1_MUX | \
    UART_DEFAULT_PINS   << UART2_MUX | \
    UART_NO_PINS        << UART3_MUX

//UART config
#define DMXUART USART0
#define USBUART USART2

#define USBPORT     PORTF
#define USB_RXC_vect USART2_RXC_vect
#define USB_TXC_vect USART2_TXC_vect
#define USB_DRE_vect USART2_DRE_vect

#define USBSPEED 115200

#define DMXPORT PORTA
#define DMX_RXC_vect USART0_RXC_vect
#define DMX_TXC_vect USART0_TXC_vect
#define DMX_DRE_vect USART0_DRE_vect
